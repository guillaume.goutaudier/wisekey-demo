//
// Imports
//
var Client = require('fabric-client');
var path = require('path');
var util = require('util');
const config = require('./config');

//
// Script configuartion variables
//
var fcn = 'set';
var args = ["a","60"];

//
// Config from network.yaml (must be downloaded from OBP)
//
var client = Client.loadFromConfig('network_org1.yaml');
var targets = client.getPeersForOrg('org1');

// 
// Main
// 
client.initCredentialStores()
.then((nothing) => {
    channel = client.getChannel(config.CHANNEL_NAME);
    //console.log(util.inspect(client));

    client.setUserContext({username: config.ADMIN_USER, password: config.ADMIN_PASSWORD})
.then((user) => {

    console.log('Using user: '+user.getName());

    tx_id = client.newTransactionID(true);

    var proposal_request = {
      targets: targets,
      chaincodeId: config.CHAINCODE_NAME,
      fcn: fcn,
      args: args,
      txId: tx_id
    };

    channel.sendTransactionProposal(proposal_request).then((response_payloads) => {
        var proposalResponses = response_payloads[0];
        var proposal = response_payloads[1];

        var transaction_request = {
          proposalResponses: proposalResponses,
          proposal: proposal,
          txId: tx_id,
        };


        console.log("Response from peers:");
        console.log(util.inspect(response_payloads));

        console.log("Transaction request sent to the orderer:");
        console.log(util.inspect(transaction_request));


        channel.sendTransaction(transaction_request).then((results) => {
          console.log('Successfully sent transaction');
        });
    });

});

});


