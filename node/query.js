//
// Imports
//
var Client = require('fabric-client');
var path = require('path');
var util = require('util')
const config = require('./config')

//
// Script configuration variables
//
var fcn = 'get';
var args = ["a"];

// Using loadFromConfig to get network topology
// https://hyperledger.github.io/fabric-sdk-node/release-1.4/tutorial-network-config.html
var client = Client.loadFromConfig('network_org1.yaml');
var targets = client.getPeersForOrg('org1');
//console.log(util.inspect(targets));

// 
// Main
// 
var request = {
  targets: targets,
  chaincodeId: config.CHAINCODE_NAME,
  fcn: fcn,
  args: args
};

client.initCredentialStores()
.then((nothing) => {
    channel = client.getChannel(config.CHANNEL_NAME);
    //console.log(util.inspect(client));

    client.setUserContext({username: config.ADMIN_USER, password: config.ADMIN_PASSWORD})
.then((user) => {

    console.log('Using user: '+user.getName());

    channel.queryByChaincode(request).then((response_payloads) => {
        for(let i = 0; i < response_payloads.length; i++) {
            console.log(util.format('Query result from peer [%s]: %s', i, response_payloads[i].toString('utf8')));
        }
    }, (err) => {
                console.log('Failed to send query due to error: ' + err.stack ? err.stack : err);
                throw new Error('Failed, got error on query');
        });

    });

});


