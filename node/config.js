const config = {
  INSTANCE_NAME: 'Demo',
  CHANNEL_NAME: 'mychannel',
  CHAINCODE_NAME: 'mycc',
  ADMIN_USER: 'admin-org1',
  ADMIN_PASSWORD: 'org1AdminPW',
  USER: 'user-org1',
  PASSWORD: 'org1UserPW',
  OFFLINE_USER: 'wisekey',
  OFFLINE_PASSWORD: 'whouCh8udEshoh9w!',
  CSR: './certs/wisekey.csr',
  CERT: './certs/wisekey.cer',
  PRIV: './certs/wisekey.pem'
};
module.exports = config;

