mkdir -p /tmp/hyperledger/org2/peer1/assets/ca
cp /tmp/hyperledger/org2/ca/crypto/ca-cert.pem /tmp/hyperledger/org2/peer1/assets/ca/org2-ca-cert.pem
export FABRIC_CA_CLIENT_HOME=/tmp/hyperledger/org2/peer1
export FABRIC_CA_CLIENT_TLS_CERTFILES=/tmp/hyperledger/org2/peer1/assets/ca/org2-ca-cert.pem
export FABRIC_CA_CLIENT_MSPDIR=msp
./bin/fabric-ca-client enroll -d -u https://peer1-org2:peer1PW@0.0.0.0:6054

mkdir -p /tmp/hyperledger/org2/peer1/assets/tls-ca
cp /tmp/hyperledger/tls-ca/crypto/ca-cert.pem /tmp/hyperledger/org2/peer1/assets/tls-ca/tls-ca-cert.pem
export FABRIC_CA_CLIENT_TLS_CERTFILES=/tmp/hyperledger/org2/peer1/assets/tls-ca/tls-ca-cert.pem
export FABRIC_CA_CLIENT_MSPDIR=tls-msp
./bin/fabric-ca-client enroll -d -u https://peer1-org2:peer1PW@0.0.0.0:6051 --enrollment.profile tls --csr.hosts peer1-org2
cd /tmp/hyperledger/org2/peer1/tls-msp/keystore
mv * key.pem
cd -
