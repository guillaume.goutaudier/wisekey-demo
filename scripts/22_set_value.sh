export CORE_PEER_ADDRESS=peer1-org1:7051
export CORE_PEER_MSPCONFIGPATH=/tmp/hyperledger/org1/admin/msp
peer chaincode invoke -C mychannel -n mycc -c '{"Args":["set","a","wisekey"]}' --tls true --cafile /tmp/hyperledger/org1/peer1/assets/tls-ca/tls-ca-cert.pem

