mkdir /tmp/hyperledger/org0/msp
mkdir /tmp/hyperledger/org0/msp/admincerts
mkdir /tmp/hyperledger/org0/msp/cacerts
mkdir /tmp/hyperledger/org0/msp/tlscacerts
mkdir /tmp/hyperledger/org0/msp/users
cp /tmp/hyperledger/org0/admin/msp/signcerts/cert.pem /tmp/hyperledger/org0/msp/admincerts/admin-org0-cert.pem
cp /tmp/hyperledger/org0/ca/crypto/ca-cert.pem /tmp/hyperledger/org0/msp/cacerts/org0-ca-cert.pem
cp /tmp/hyperledger/tls-ca/crypto/ca-cert.pem /tmp/hyperledger/org0/msp/tlscacerts/tls-ca-cert.pem

mkdir /tmp/hyperledger/org1/msp
mkdir /tmp/hyperledger/org1/msp/admincerts
mkdir /tmp/hyperledger/org1/msp/cacerts
mkdir /tmp/hyperledger/org1/msp/tlscacerts
mkdir /tmp/hyperledger/org1/msp/users
cp /tmp/hyperledger/org1/admin/msp/signcerts/cert.pem /tmp/hyperledger/org1/msp/admincerts/admin-org1-cert.pem
cp /tmp/hyperledger/org1/ca/crypto/ca-cert.pem /tmp/hyperledger/org1/msp/cacerts/org1-ca-cert.pem
cp /tmp/hyperledger/tls-ca/crypto/ca-cert.pem /tmp/hyperledger/org1/msp/tlscacerts/tls-ca-cert.pem

mkdir /tmp/hyperledger/org2/msp
mkdir /tmp/hyperledger/org2/msp/admincerts
mkdir /tmp/hyperledger/org2/msp/cacerts
mkdir /tmp/hyperledger/org2/msp/tlscacerts
mkdir /tmp/hyperledger/org2/msp/users
cp /tmp/hyperledger/org2/admin/msp/signcerts/cert.pem /tmp/hyperledger/org2/msp/admincerts/admin-org2-cert.pem
cp /tmp/hyperledger/org2/ca/crypto/ca-cert.pem /tmp/hyperledger/org2/msp/cacerts/org2-ca-cert.pem
cp /tmp/hyperledger/tls-ca/crypto/ca-cert.pem /tmp/hyperledger/org2/msp/tlscacerts/tls-ca-cert.pem

