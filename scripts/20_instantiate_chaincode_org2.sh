export CORE_PEER_ADDRESS=peer2-org2:10051
export CORE_PEER_MSPCONFIGPATH=/tmp/hyperledger/org2/admin/msp
peer chaincode instantiate -C mychannel -n mycc -v 1.0 -c '{"Args":["init"]}' -o orderer1-org0:7050 --tls --cafile /tmp/hyperledger/org2/peer1/tls-msp/tlscacerts/tls-0-0-0-0-6051.pem

