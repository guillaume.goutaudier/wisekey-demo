// Imports
var Client = require('fabric-client');
var util = require('util');
var fs = require('fs');
const config = require('./config');

// Config from network.yaml (must be downloaded from OBP)
var client = Client.loadFromConfig('network.yaml');

// New user enrollment ID and enrollment secret that can be 
// generated with register.js
var fabric_ca_client = null;
var csr = fs.readFileSync(config.CSR, 'utf8');
var req = {
    enrollmentID: config.OFFLINE_USER,
    enrollmentSecret: config.OFFLINE_PASSWORD,
    csr: csr,
};


// Main
client.initCredentialStores()
.then((nothing) => {
    //console.log(util.inspect(client));

    fabric_ca_client = client.getCertificateAuthority();

    console.log('Sending enrollment request');
    
    return fabric_ca_client.enroll(req);
    })
.then((response) => {
        console.log(util.inspect(response));
        console.log('Enrollment Certificate: '+response.certificate);
    });


