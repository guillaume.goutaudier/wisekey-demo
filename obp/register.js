// Imports
var Client = require('fabric-client');
var util = require('util');
const config = require('./config');

// Config from network.yaml (must be downloaded from OBP)
var client = Client.loadFromConfig('network.yaml');
var client_org = client.getClientConfig().organization;
var fabric_ca_client = null;

// Main
client.initCredentialStores()
.then((nothing) => {
    fabric_ca_client = client.getCertificateAuthority();

    return client.setUserContext({username: config.OBP_USER, password: config.OBP_PASSWORD})
    })
.then((admin) => {

    console.log('Running registration query with this admin user: '+admin.getName());

     return fabric_ca_client.register({enrollmentID: config.OFFLINE_USER, enrollmentSecret: config.OFFLINE_PASSWORD, role: 'client', affiliation: client_org}, admin)
    })
.then((secret) => {
        console.log('Secret to use for '+config.OFFLINE_USER+':'+secret);
    });


