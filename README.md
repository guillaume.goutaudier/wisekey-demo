# Hyperledger Fabric offline signatures with WISeKey secure devices
The objective of this demo is to record a Hyperledger Fabric transation using an offline signature created by a WISeKey secure chipset.
In this readme we will go thorugh all the installation steps, so one can easily replay this demo.

Here is a high-level view of the architecture we will create:
![architecture.png](pics/architecture.png)


# Install dependencies
### Node.js installation
We will use the Node.js SDK, as it seems to be the more mature SDK. The offline signature mechanism is documented here:
https://fabric-sdk-node.github.io/release-1.4/tutorial-sign-transaction-offline.html

Officially, the Fabric node SDK is supported with node 8.x, but we will use version 10 which should run fine.

On Mac, you can use brew to install it:
```
% brew install node@10
% node -v
v10.18.0
% npm -v
6.13.4
```

On Linux Ubuntu 18.04:
```
% curl https://raw.githubusercontent.com/creationix/nvm/master/install.sh | bash
# Restart a terminal session to reload environment variables
% nvm install v10.18.1
% node -v
v10.18.1
% npm -v
6.13.4
```

### Fabric binaries
Clone the gitlab repo and execute all the following commands from the `wisekey-demo` directory:
```
% git clone https://gitlab.com/guillaume.goutaudier/wisekey-demo.git
% cd wisekey-demo
```

Download the Fabric and Fabric CA binaries:
```
# On Mac:
. ./scripts/download_mac_binaries.sh
# On Linux:
. ./scripts/download_linux_binaries.sh
```

# Create network with Fabric-CA
In this section we will follow the instructions from this guide:
https://hyperledger-fabric-ca.readthedocs.io/en/latest/operations_guide.html

Versions used for the test:
- docker 19.03.5
- docker-compose 1.25.3
- go 1.13.6
- fabric-ca-client 1.4.4

On Linux Ubuntu 18.04, follow the instructions from the below URLs to install docker and docker-compose:
- https://docs.docker.com/install/linux/docker-ce/ubuntu/
- https://docs.docker.com/compose/install/

On Linux Ubuntu 18.04, installing go 1.13 can be done with this command:
```
sudo snap install --classic go
```

### Start CAs
```
% docker-compose -f docker/docker-compose.yml up -d ca-tls rca-org0 rca-org1 rca-org2
```

### Register peers and orderer with TLS CA
```
# On Mac:
% . ./scripts/1_register_tls.sh
# On  Linux:
% sudo /bin/bash ./scripts/1_register_tls.sh
```
This script will enroll with the admin account and register the following entities:

| Entitiy       | Secret    |
| ---           | ---       |
| peer1-org1    | peer1PW   | 
| peer2-org1    | peer2PW   |
| peer1-org2    | peer1PW   |
| peer2-org2    | peer2PW   |
| orderer1-org0 | ordererPW | 
 

### Register admin and orderer with Org0 CA
```
# On Mac:
. ./scripts/2_register_org0.sh
# On Linux:
% sudo /bin/bash ./scripts/2_register_org0.sh
```
This script will enroll with the admin account and register the following entities:

| Entitiy       | Secret      |
| ---           | ---         |
| orderer1-org0 | ordererpw   |
| admin-org0    | org0adminpw |   

### Register admin and peers with Org1 CA
```
# On Mac:
% . ./scripts/3_register_org1.sh
# On Linux:
% sudo /bin/bash ./scripts/3_register_org1.sh
```
This script will enroll with the admin account and register the following entities:

| Entitiy       | Secret      |
| ---           | ---         |
| peer1-org1    | peer1PW     |
| peer2-org1    | peer2PW     |   
| admin-org1    | org1AdminPW |

### Register admin and peers with Org2 CA
```
# On Mac:
% . ./scripts/4_register_org2.sh
# On Linux:
% sudo /bin/bash ./scripts/4_register_org2.sh
```
This script will enroll with the admin account and register the following entities:

| Entitiy       | Secret      |
| ---           | ---         |
| peer1-org2    | peer1PW     |
| peer2-org2    | peer2PW     |
| admin-org2    | org2AdminPW |

### Enroll Org1 peers with Org1 CA and TLS CA
```
# On Mac:
% . ./scripts/5_enroll_peer1_org1.sh
% . ./scripts/6_enroll_peer2_org1.sh
# On Linux:
% sudo /bin/bash ./scripts/5_enroll_peer1_org1.sh
% sudo /bin/bash ./scripts/6_enroll_peer2_org1.sh
```

### Enroll Org1 Admin and move certificate to peers
```
# On Mac:
% . ./scripts/7_enroll_admin_org1.sh
# On Linux:
% sudo /bin/bash ./scripts/7_enroll_admin_org1.sh
```

### Start Org1 peers
```
% docker-compose -f docker/docker-compose.yml up -d peer1-org1 peer2-org1
```

### Enroll Org2 peer1 with Org2 CA and TLS CA
```
# On Mac:
% . ./scripts/8_enroll_peer1_org2.sh
% . ./scripts/9_enroll_peer2_org2.sh
# On Linux:
% sudo /bin/bash ./scripts/8_enroll_peer1_org2.sh
% sudo /bin/bash ./scripts/9_enroll_peer2_org2.sh
```

### Enroll Org2 Admin and move certificate to peers
```
# On Mac:
% . ./scripts/10_enroll_admin_org2.sh
# On Linux:
% sudo /bin/bash ./scripts/10_enroll_admin_org2.sh
```

### Start Org2 peers
```
% docker-compose -f docker/docker-compose.yml up -d peer1-org2 peer2-org2
```


### Enroll Orderer
```
# On Mac:
% . ./scripts/11_enroll_orderer.sh
# On Linux:
% sudo /bin/bash ./scripts/11_enroll_orderer.sh
```

### Enroll Org0 Admin and move certificate to orderer
```
# On Mac:
% . ./scripts/12_enroll_admin_org0.sh
# On Linux:
% sudo /bin/bash ./scripts/12_enroll_admin_org0.sh
```

### Create Genesis Block
Prepare the MSP directories and generate the genesis block:
```
# On Mac:
% . ./scripts/13_prepare_msp_directories.sh
% . ./scripts/14_create_genesis_block.sh
# On Linux:
% sudo /bin/bash ./scripts/13_prepare_msp_directories.sh
% sudo /bin/bash ./scripts/14_create_genesis_block.sh
```

### Start Orderer and CLIs
```
% docker-compose -f docker/docker-compose.yml up -d orderer1-org0 cli-org1 cli-org2
```

### Create and join channel
```
# On Mac:
% . ./scripts/15_prepare_artifacts.sh 
# On Linux:
% sudo /bin/bash ./scripts/15_prepare_artifacts.sh
```

```
% docker exec -it cli-org1 sh /tmp/hyperledger/org1/admin/scripts/16_create_channel.sh
... Successfully submitted proposal to join channel
```

```
# On Mac:
% cp /tmp/hyperledger/org1/peer1/assets/mychannel.block /tmp/hyperledger/org2/peer1/assets/mychannel.block
# On Linux:
% sudo cp /tmp/hyperledger/org1/peer1/assets/mychannel.block /tmp/hyperledger/org2/peer1/assets/mychannel.block
% docker exec -it cli-org2 sh /tmp/hyperledger/org2/admin/scripts/17_join_channel.sh
... Successfully submitted proposal to join channel
```

### Deploy chaincode
```
% docker exec -it cli-org1 sh /tmp/hyperledger/org1/admin/scripts/18_install_chaincode_org1.sh
... Installed remotely response:<status:200 payload:"OK" >
```

```
% docker exec -it cli-org2 sh /tmp/hyperledger/org2/admin/scripts/19_install_chaincode_org2.sh
... Installed remotely response:<status:200 payload:"OK" >
% docker exec -it cli-org2 sh /tmp/hyperledger/org2/admin/scripts/20_instantiate_chaincode_org2.sh
```

### Interact with the chaincode
Query and set value from peer1-org1:
```
% docker exec -it cli-org1 sh /tmp/hyperledger/org1/admin/scripts/21_query_value.sh
... 10
% docker exec -it cli-org1 sh /tmp/hyperledger/org1/admin/scripts/22_set_value.sh
... Chaincode invoke successful. result: status:200 payload:"wisekey" 
```

Query value from peer1-org2:
```
% docker exec -it cli-org2 sh /tmp/hyperledger/org2/admin/scripts/23_query_value.sh
... wisekey
```

# Node SDK
Now that we have a working environment we will use the node SDK to interact with the Blockchain network.

First install the fabric-client module:
```
% sudo apt-get install build-essential << Linux only
% cd node
% npm install fabric-client@1.4.4
```

### First tests
Validate that the SDK is working properly with these sample scripts:
```
% node query.js
Using user: admin-org1
Query result from peer [0]: wisekey
Query result from peer [1]: wisekey
% node invoke.js
... Successfully sent transaction
% node query.js
Using user: admin-org1
Query result from peer [0]: 60
Query result from peer [1]: 60
```

### Offline signatures
We first need to create a new user private key and certificate signing request (CSR). Go to the 'certs' directory and run the following command (when asked for the common name make sure this is the same as the enrollment ID):
```
% mkdir certs
% cd certs
% openssl ecparam -name prime256v1 -genkey -noout -out wisekey_nopassphrase.pem
% openssl ec -aes-128-cbc -in wisekey_nopassphrase.pem -out wisekey.pem -passout pass:wisekey
% openssl req -new -key wisekey.pem -out wisekey.csr -passin pass:wisekey -subj "/C=CH/O=org1/CN=wisekey"
% cd ..
```

Then we register and enroll a new 'wisekey' user with this CSR: 
```
% cd ..
# On Linux:
% sudo sh ./scripts/24_register_offline_user.sh
% cd node
% node enroll.js
... 
Enrollment Certificate: -----BEGIN CERTIFICATE-----
...
-----END CERTIFICATE-----
# save the certificate to certs/wisekey.cer
```

We can now execute the transaction with offline signatures:
```
% node invoke_offline.js
...
Successfully sent transaction
Return code: SUCCESS
```

### Clean-up
```
docker-compose down
docker system prune --volumes
rm -rf /tmp/hyperledger /tmp/hfc-cvs /tmp/hfc-kvs
```

# Using the Oracle Blockchain Platform
### Node SDK installation
On Mac OS, make sure xcode is installed:
```
xcode-select --install
```

In the Oracle Blockchain Platform Console, go to "Developer Tools / Application Development" and download the automation script: `npm_bcs_client.sh`. Move this script to the `obp` directory. 

This script will essentially install the fabric-client Node SDK. It is important that you install it using this script: some tunings are required and the default installation will not work.

We will use the version 1.4.1 of the fabric-client SDK, which is the latest supported version. For the fabric-ca-client command line utility, it is fine to use version 1.4.4 that was previously downloaded.

Edit this script and replace the NPM_PACKAGE_VER variable by @1.4.1:
```
NPM_PACKAGE_VER=@1.4.1
```

Execute it and make sure there are no errors:
```
% cd obp
% /bin/bash npm_bcs_client.sh
```

Note: if you get an error while installting 'gyp', you might need to reinstall xcode:
```
% sudo rm -rf $(xcode-select -print-path)
% xcode-select --install
```

### Download artifacts and enroll as registrar
The Node.js SDK client can easily be configured using a set of artifacts and a configuration file that can be downloaded from the Oracle Blockchain Platform.

In the Oracle Blockchain Platform Console, go to "Developer Tools / Application Development" and download the OBP development package. Then unzip it and move all its content (artifacts directory and network.yaml file) to the `obp` directory.

If you inspect the `network.yaml` file, you will see that the CA has a registrar that corresponds to the Oracle Cloud identity that you used to create the Blockchain environment. The first thing we will do is to enroll this identity. This will populate the `ca-admin` directory with a new private key and certificate that fabric-ca-client will use for all the futur requests:
```
% cd ..
% export OBP_USER=<YOUR ORACLE CLOUD USER>
% export OBP_PASSWORD=<YOUR ORACLE CLOUD PASSWORD>
% export OBP_INSTANCE=<NAME OF YOUR OBP INSTANCE>
% export OBP_CA_URL=<CA URL FROM THE NODES TAB>
% export FABRIC_CA_CLIENT_TLS_CERTFILES=$PWD/obp/artifacts/crypto/peerOrganizations/$OBP_INSTANCE/tlscacert/$OBP_INSTANCE-tlscacerts.pem
% export FABRIC_CA_CLIENT_HOME=$PWD/obp/ca-admin
% ./bin/fabric-ca-client enroll -u https://$OBP_USER:$OBP_PASSWORD@$OBP_CA_URL
```

### Validate the newly created user can query the Blockchain
Register a new "demouser" user:
```
# If using OBPEE, create the user in LDAP first with the same secret
% ./bin/fabric-ca-client register --id.name 'demouser' --id.secret 'zigaiL3UShoo3fee!' --id.type client -u https://$OBP_CA_URL
```

Now that our new user is registered, we can query the installed chaincode (note here that the enrollment request is done dynamically in the script):
```
% cd obp
% node query.js 
...
Using user: demouser
Query result from peer [0]: 10
Query result from peer [1]: 10
```

We can also invoke the chaincode to modify the value:
```
% node invoke.js 
...
Successfully sent transaction
node query.js
...
Using user: demouser
Query result from peer [0]: 60
Query result from peer [1]: 60
% cd ..
```

### Execute a transaction offline
First we register the "wisekey" offline user:
```
% export OBP_CA_URL=<CA URL FROM THE NODES TAB>
% export FABRIC_CA_CLIENT_TLS_CERTFILES=$PWD/obp/artifacts/crypto/peerOrganizations/$OBP_INSTANCE/tlscacert/$OBP_INSTANCE-tlscacerts.pem
% export FABRIC_CA_CLIENT_HOME=$PWD/ca-admin
% export OFFLINE_USER=wisekey
# If using OBPEE, create the user in LDAP first with the same secret
% ./bin/fabric-ca-client register --id.name $OFFLINE_USER --id.secret 'whouCh8udEshoh9w!' --id.type client -u https://$OBP_CA_URL
```

Now we generate the crypo material for the 'wisekey' user:
```
% cd obp
% mkdir certs
% cd certs
% openssl ecparam -name prime256v1 -genkey -noout -out wisekey_nopassphrase.pem
% openssl ec -aes-128-cbc -in wisekey_nopassphrase.pem -out wisekey.pem -passout pass:wisekey
% openssl req -new -key wisekey.pem -out wisekey.csr -passin pass:wisekey -subj "/CN=$OFFLINE_USER"
% cd ..
```

Then we can enroll this identity usign the username, password, and CSR:
```
# Edit config.js and update the value of OFFLINE_USER if necessary
% node enroll.js
# Save the output certificate to certs/wisekey.cer
```

Finally we can execute an offline transaction:
```
% node invoke_offline.js
...
Successfully sent transaction
Return code: SUCCESS
```
